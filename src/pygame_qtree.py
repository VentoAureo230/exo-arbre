import pygame
from quadtree import QuadTree


class PyGame:
    def __init__(self, screen_width, screen_height, bgcolor=(110, 110, 255)):
        """
        :param screen_width: Widht of the window
        :param screen_height: Height of the window
        :param bgcolor: Backgroundcolor of the window
        """
        self.screen_width = screen_width
        self.screen_height = screen_height
        self.screen = pygame.display.set_mode((screen_width, screen_height))
        self.color = self.screen.fill(bgcolor)

    def display(self, quadtree):
        """
        :param quadtree: Represents the Quadtree structure
        :return: Provides a display specified by quadtree parameter
        """
        pygame.init()
        pygame.display.set_caption("Quadtree")

        run = True
        while run:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    run = False
            self.draw_quadtree(quadtree)
            pygame.display.update()
        pygame.quit()

    def draw_quadtree(self, quadtree, x=0, y=0, width=None, height=None):
        """
        :param quadtree: Represents a node or subtree
        :param x & y: Coords of the topleft corner
        :param width & height: Width & height of the rectangle representing current Quadtree node
        :return: When called recursively draws rectangles representing each node or subtree. Rectangle are outlined by a purple color
        """
        if width is None:
            width = self.screen_width
        if height is None:
            height = self.screen_height

        if isinstance(quadtree, QuadTree):
            quad_width = width / 2
            quad_height = height / 2

            self.draw_quadtree(quadtree.topLeft, x, y, quad_width, quad_height)
            self.draw_quadtree(quadtree.topRight, x + quad_width, y, quad_width, quad_height)
            self.draw_quadtree(quadtree.bottomLeft, x, y + quad_height, quad_width, quad_height)
            self.draw_quadtree(quadtree.bottomRight, x + quad_width, y + quad_height, quad_width, quad_height)

            pygame.draw.rect(self.screen, (158, 36, 250), (x, y, width, height), 2)


if __name__ == "__main__":
    pygame_instance = PyGame(800, 600)

    # TODO : Add a file picker
    quadtree = QuadTree.from_file('files/quadtree.json')

    pygame_instance.display(quadtree)
