from __future__ import annotations

import json


class QuadTree:
    @property
    def bottomRight(self):
        return self._bottomRight

    @property
    def bottomLeft(self):
        return self._bottomLeft

    @property
    def topRight(self):
        return self._topRight

    @property
    def topLeft(self):
        return self._topLeft

    NB_NODES: int = 4

    def __init__(self,
                 topLeft: bool | QuadTree,
                 topRight: bool | QuadTree,
                 bottomLeft: bool | QuadTree,
                 bottomRight: bool | QuadTree
                 ):
        """
        :param topLeft: top left of the node or subtree
        :param topRight: top right of the node or subtree
        :param bottomLeft: bottom left of the node or subtree
        :param bottomRight: bottom right of the node or subtree
        """
        self.topLeft = topLeft
        self.topRight = topRight
        self.bottomLeft = bottomLeft
        self.bottomRight = bottomRight

    @property
    def depth(self) -> int:
        """
        Returns the depth of the quadtree (int)
        """
        if not isinstance(self.topLeft, QuadTree) and not isinstance(self.topRight, QuadTree) and not isinstance(
                self.bottomLeft, QuadTree) and not isinstance(self.bottomRight, QuadTree):
            return 1
        else:
            depth = []
            for i in [self.topLeft, self.topRight, self.bottomLeft, self.bottomRight]:
                if isinstance(i, QuadTree):
                    depth.append(i.depth)
            return max(depth) + 1

    def from_file(filename: str) -> QuadTree:
        """ Open a given file, containing a textual representation of a list"""
        with open(filename, 'r') as file:
            data = json.load(file)
            return QuadTree.fromList(data)

    @staticmethod
    def fromList(data: list) -> QuadTree:
        """
        Match statement to check each element of the list.
        :param data: Elements of the list are topLeft, topRight, bottomLeft, bottomRight
        :return: QuadTree
        """
        if len(data) != 4:
            raise ValueError("Invalid quadtree size")
        topLeft, topRight, bottomLeft, bottomRight = data

        match bottomRight:
            case list():
                bottomRight = QuadTree.fromList(bottomRight)
            case 0:
                bottomRight = False
            case 1:
                bottomRight = True

        match bottomLeft:
            case list():
                bottomLeft = QuadTree.fromList(bottomLeft)
            case 0:
                bottomLeft = False
            case 1:
                bottomLeft = True

        match topRight:
            case list():
                topRight = QuadTree.fromList(topRight)
            case 0:
                topRight = False
            case 1:
                topRight = True

        match topLeft:
            case list():
                topLeft = QuadTree.fromList(topLeft)
            case 0:
                topLeft = False
            case 1:
                topLeft = True

        return QuadTree(topLeft, topRight, bottomLeft, bottomRight)

    @bottomRight.setter
    def bottomRight(self, value):
        self._bottomRight = value

    @bottomLeft.setter
    def bottomLeft(self, value):
        self._bottomLeft = value

    @topRight.setter
    def topRight(self, value):
        self._topRight = value

    @topLeft.setter
    def topLeft(self, value):
        self._topLeft = value
