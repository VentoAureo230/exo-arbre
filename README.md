# Exercice : Arbre quaternaire

Exercice Python avec PyGame permettant la visualisation graphique d'un Quadtree.

## Contenu

- [Installation](#installation)
- [Utilisation](#utilisation)

## Installation

1. [Python](https://www.python.org/) 3+ est requis pour démarrer le projet

2. Clonez ce dépôt


3. Exécutez cette commande pour installer les dépendaces du projet.
```shell
pip install -r requirements.txt
```

## Utilisation

Pour lancer l'application avec le quadtree par défaut :

Windows :
```shell
python .\src\pygame_qtree.py
```

Linux :
```shell
python src/pygame_qtree.py
```
Si vous lancez l'application de base vous obtenez ce résultat :

![example](files/example.png)

Pour changer de quadtree, éditez la ligne 62 dans [pygame_qtree.py](src/pygame_qtree.py) et ajoutez le fichier que vous voulez visualiser (préalablement présent dans le dossier [files](files/))

```python
# TODO : Add a file picker
quadtree = QuadTree.from_file('files/quadtree.json')

# TODO : Add a file picker
quadtree = QuadTree.from_file('/files/mon_fichier_quadtree.json')
```

### Upgrades :

- Add a file picker
- Make a step-by-step method to display the quadtree generation
- Use Setters properties to allow user to create a leaf location when he clicks on screen